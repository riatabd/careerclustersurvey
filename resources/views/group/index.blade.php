@extends('layouts.master')

@section('content')

    <h1>Group <a href="{{ url('group/create') }}" class="btn btn-primary pull-right btn-sm">Add New Group</a></h1>
    <div class="table">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>S.No</th><th>Name</th><th>Category</th><th>Icon</th><th>Actions</th>
                </tr>
            </thead>
            <tbody>
            {{-- */$x=0;/* --}}
            @foreach($group as $item)
                {{-- */$x++;/* --}}
                <tr>
                    <td>{{ $x }}</td>
                    <td><a href="{{ url('group', $item->id) }}">{{ $item->name }}</a></td>    
                    <td>
                    {{ $category = $item->category }}
                    @if($category == '1')
                        Activity
                    @elseif($category == '2')
                        Characteristics
                    @elseif($category == '3')
                        School Subjects
                    @endif


                    </td>
                    <td>{{ $item->icon }}</td>
                    <td>
                        <a href="{{ url('group/' . $item->id . '/edit') }}">
                            <button type="submit" class="btn btn-primary btn-xs">Update</button>
                        </a> /
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['group', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination"> {!! $group->render() !!} </div>
    </div>

@endsection
