@extends('layouts.master')

@section('content')

    <h1>Career</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Title</th><th>Icon</th><th>Q1</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $career->id }}</td> <td> {{ $career->title }} </td><td> {{ $career->icon }} </td><td> {{ $career->q1 }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection