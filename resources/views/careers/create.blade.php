@extends('layouts.master')

@section('content')

    <h1>Create New Career</h1>
    <hr/>

    {!! Form::open(['url' => 'careers', 'class' => 'form-horizontal']) !!}

                <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
                {!! Form::label('title', 'Title: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

 <div class="form-group  {{ $errors->has('studies_id') ? 'has-error' : '' }}">
            {!! Form::label('studies_id', 'School Subject: ', ['class' => 'col-sm-3 control-label']) !!}
            <div class="col-sm-6">
                {!! Form::select('studies_id', $schools, null, array('class' => 'form-control')) !!}

                {!! $errors->first('studies_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

            <div class="form-group {{ $errors->has('icon') ? 'has-error' : ''}}">
                {!! Form::label('icon', 'Icon: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('icon', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('icon', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('q1') ? 'has-error' : ''}}">
                {!! Form::label('q1', 'What do Construction Laborers and Helpers do? ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('q1', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('q1', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('q2') ? 'has-error' : ''}}">
                {!! Form::label('q2', 'What Education or Training do I Need After High School? ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('q2', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('q2', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('q3') ? 'has-error' : ''}}">
                {!! Form::label('q3', 'Do I Need to Be Certified or Licensed? ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('q3', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('q3', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('q4') ? 'has-error' : ''}}">
                {!! Form::label('q4', 'What do I Need to be Good at the Job? ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('q4', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('q4', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('q5') ? 'has-error' : ''}}">
                {!! Form::label('q5', 'What Does the Workplace Look Like? ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('q5', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('q5', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('q6') ? 'has-error' : ''}}">
                {!! Form::label('q6', 'What is the Job Outlook for This Job? ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('q6', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('q6', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('q7') ? 'has-error' : ''}}">
                {!! Form::label('q7', 'How Can I Advance in the Profession? ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('q7', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('q7', '<p class="help-block">:message</p>') !!}
                </div>
            </div>


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@endsection