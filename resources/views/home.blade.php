@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row">

        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                <div class="row">
                    <div class="col-md-5">
                        <p>
                        <a href="/studies" type="button" class="btn btn-primary btn-lg">
                        <span class="glyphicon glyphicon-plus"></span> &nbsp;
                        Create Study Programme
                        </a>
                      </p>
                    </div>
                    <div class="col-md-5">
                        <p>
                            <h4>Create Study programme for career</h4>
                      </p>
                    </div>
                    
                </div>
                 <div class="row">
                 <div class="col-md-5">
                    <p>
                    <a href="/group" type="button" class="btn btn-primary btn-lg">
                    <span class="glyphicon glyphicon-plus"></span> &nbsp;
                    Create Group Option
                    </a>
                  </p>
                </div>
                    <div class="col-md-5">
                        <p>
                            <h4>Create Item for selecting to compare careers</h4>
                      </p>
                    </div>
                </div>

                <div class="row">
                 <div class="col-md-5">
                    <p>
                    <a href="/careers" type="button" class="btn btn-primary btn-lg">
                    <span class="glyphicon glyphicon-plus"></span> &nbsp;
                    Create Sub Studies programme
                    </a>
                  </p>
                </div>
                    <div class="col-md-5">
                        <p>
                            <h4>Create sub studies more careers</h4>
                      </p>
                    </div>
                </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
