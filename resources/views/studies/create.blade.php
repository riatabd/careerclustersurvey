@extends('layouts.master')

@section('content')

    <h1>Create New Study</h1>
    <hr/>

    {!! Form::open(['url' => 'studies', 'class' => 'form-horizontal']) !!}

                <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', 'Name: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-7">
                    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            
            <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                {!! Form::label('description', 'Description: ', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-7">
                    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            
            <div class="row" style="margin-bottom: 20px;">
            <b style="font-size: 2em;background: #2C3E50;padding: 10px;color: #fff;">Activity</b>
            <hr />
            @foreach($activity as $active => $ac)
                <div class="col-sm-7">
                    {!! Form::label('groups', $ac) !!} &nbsp;
                    {!! Form::checkbox('groups[]', $active, null) !!}
                </div>
            @endforeach
            </div>
            
            <div class="row" style="margin-bottom: 20px;">
            <b style="font-size: 2em;background: #2C3E50;padding: 10px;color: #fff;">Characteristics</b>
            <hr />
            @foreach($characteristics as $characteristic => $ch)
                <div class="col-sm-7">
                    {!! Form::label('groups', $ch) !!} &nbsp;
                    {!! Form::checkbox('groups[]', $characteristic, null) !!}
                </div>
            @endforeach
            </div>
            
            <div class="row" style="margin-bottom: 20px;">
            <b style="font-size: 2em;background: #2C3E50;padding: 10px;color: #fff;">School Subjects</b>
            <hr />
            @foreach($subjects as $subject => $sb)
                <div class="col-sm-7">
                    {!! Form::label('groups', $sb) !!} &nbsp;
                    {!! Form::checkbox('groups[]', $subject, null) !!}
                </div>
            @endforeach
            </div>
            
            

            


    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

@endsection