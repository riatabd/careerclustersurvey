@extends('layouts.master')

@section('content')

    <h1>Study</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Name</th><th>Icon</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $study->id }}</td> <td> {{ $study->name }} </td><td> {{ $study->icon }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection