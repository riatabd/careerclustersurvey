<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddtoGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('studies', function(Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->text('description');
                $table->timestamps();
            });
            
        Schema::create('groups', function(Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('icon');
                $table->enum('category', ['1', '2', '3']);
                $table->timestamps();
            });

        Schema::create('group_study', function(Blueprint $table) {
                    $table->integer('group_id')->unsigned();
                    $table->integer('studies_id')->unsigned();
                    $table->foreign('studies_id')->references('id')->on('studies')->onDelete('restrict');
                    $table->foreign('group_id')->references('id')->on('groups')->onDelete('restrict');
                });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('studiesgroup');
        Schema::drop('group');
        Schema::drop('studies');
    }
}
