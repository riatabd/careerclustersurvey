<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddCareers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
            Schema::create('careers', function(Blueprint $table) {
                $table->increments('id');
                $table->string('title');
                $table->integer('studies_id')->unsigned();
$table->string('icon');
$table->text('q1');
$table->text('q2');
$table->text('q3');
$table->text('q4');
$table->text('q5');
$table->text('q6');
$table->text('q7');

$table->foreign('studies_id')->references('id')->on('studies')->onDelete('restrict');

                $table->timestamps();
            });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('careers');
    }

}
