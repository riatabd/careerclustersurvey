<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Study extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'studies';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description'];

    public function group()
    {
        return $this->belongsToMany('App\Group');
    }

    public function careers()
    {
        return $this->hasMany('App\Career');
    }

}
