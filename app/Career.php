<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Career extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'careers';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'studies_id', 'icon', 'q1', 'q2', 'q3', 'q4', 'q5', 'q6', 'q7'];

    public function studies()
    {
        return $this->belongsTo('App\Study');
    }

}
