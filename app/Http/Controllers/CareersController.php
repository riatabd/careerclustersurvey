<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Career;
use App\Study;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class CareersController extends Controller
{

public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $careers = Career::paginate(15);

        return view('careers.index', compact('careers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $schools = Study::lists('name', 'id')->toArray();
        return view('careers.create', compact('schools'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['title' => 'required', 'icon' => 'required', ]);

        Career::create($request->all());

        Session::flash('flash_message', 'Career added!');

        return redirect('careers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $career = Career::findOrFail($id);

        return view('careers.show', compact('career'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $career = Career::findOrFail($id);
        $schools = Study::lists('name', 'id')->toArray();
        return view('careers.edit', compact('career','schools'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['title' => 'required', 'icon' => 'required', ]);

        $career = Career::findOrFail($id);
        $career->update($request->all());

        Session::flash('flash_message', 'Career updated!');

        return redirect('careers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Career::destroy($id);

        Session::flash('flash_message', 'Career deleted!');

        return redirect('careers');
    }

}
