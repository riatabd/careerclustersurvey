<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Study;
use App\Group;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class StudiesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $studies = Study::paginate(20);

        return view('studies.index', compact('studies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        
        $activity = Group::where('category', '1')->orderBy('name')->lists('name', 'id');
        $characteristics = Group::where('category', '2')->orderBy('name')->lists('name', 'id');
        $subjects = Group::where('category', '3')->orderBy('name')->lists('name', 'id');
        
        return view('studies.create', compact('activity','characteristics','subjects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);
        
        $studies = new Study();
        $studies -> name = $request->name;
        $studies -> description = $request->description;


        
        /*        
        $studies_id = $studies -> id;
        $studies->group()->attach($group_id, $studies_id);
        */
        

        //Study::create($request->all());
        $studies->save();
        
        $groups = $studies -> groups = $request->groups;
        //$studies->group()->attach($groups, $studies->id);
        
        foreach($groups as $group)
        {
            DB::insert('INSERT INTO group_study (group_id, studies_id) VALUES (?, ?)', array($group, $studies->id));    
            
        }

        
        
        //print_r($groups);
        return redirect('studies');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $study = Study::findOrFail($id);

        return view('studies.show', compact('study'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $study = Study::findOrFail($id);

        return view('studies.edit', compact('study'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        $study = Study::findOrFail($id);
        $study->update($request->all());

        Session::flash('flash_message', 'Study updated!');

        return redirect('studies');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Study::destroy($id);

        Session::flash('flash_message', 'Study deleted!');

        return redirect('studies');
    }

}
