<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});

Route::group(['middleware' => ['web']], function () {
	Route::auth();
	
	Route::get('/', function () {
		 if (Auth::guest()) 
		 	{
		 		return Redirect::guest('login');
		 	}
		 	else {
		 		return view('home');
		 	}
    	//return view('home');
	});


	Route::get('/home', 'HomeController@index');

	Route::resource('group', 'GroupController');
	Route::resource('studies', 'StudiesController');
	Route::resource('careers', 'CareersController');
    
});

